from functions import m_json
from functions import m_pck


#Funktion zum Starten mit übergabe des Pfades der jeweiligen Setup Datei und Pfad zum speichern der Ergebnisse
def start_experiment (path, data_folder)-> None:
    
    #Pfade zu datein
    folder_path = "/home/pi/calorimetry_home/datasheets/"
    json_folder = "/home/pi/calorimetry_home/datasheets/"
    
    # Metadaten aus Setup datei auslesen
    metadata = m_json.get_metadata_from_setup(path)

    print(metadata)

    # Seriennummer Sensoren hinzufügen

    m_json.add_temperature_sensor_serials(folder_path, metadata)

    # Daten Archivieren
    m_json.archiv_json(folder_path, path, data_folder)

    # Sensoren überprüfen

    #m_pck.check_sensors()

    # Messdatensammeln

    data = m_pck.get_meas_data_calorimetry(metadata)

    # Messdaten in H5 Datei speichern

    m_pck.logging_calorimetry(
        data,
        metadata,
        data_folder,
        json_folder)



#für Versuch Newton
start_experiment("/home/pi/calorimetry_home/datasheets/setup_newton.json", "/home/pi/calorimetry_home/experiment_newton")

#für Versuch Kapazität
#start_experiment("/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json", "/home/pi/calorimetry_home/experiment_capacity")
     